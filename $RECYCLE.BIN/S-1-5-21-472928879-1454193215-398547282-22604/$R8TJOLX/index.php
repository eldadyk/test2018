<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());

$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{number}', function($request, $response,$args){
   return $response->write('The customer number is: '.$args['number']);
});
$app->get('/customers/{number}/products/{pnumber}', function($request, $response,$args){
   return $response->write('customer number '.$args['number'].' ordered product number '.$args['pnumber']);
});
$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages = $_message->all(); //מערך של אוביקטים 
   $payload = [];
   foreach($messages as $msg){
       $payload[$msg->id] = [
           'body'=>$msg->body,
           'user_id'=>$msg->user_id,
           'created_at'=>$msg->created_at
       ];        
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->get('/messages/{id}', function($request, $response,$args){
   $_id = $args['id']; 
   $message = Message::find($_id); 
   return $response->withStatus(200)->withJson($message);
});

$app->post('/messages', function($request, $response,$args){
   $message = $request->getParsedBodyParam('message', '');
   $user_id = $request->getParsedBodyParam('user_id', '');
   $_message = new Message();
   $_message->body = $message; 
   $_message->user_id = $user_id;
   $_message->save();
   if($_message->id){
       $payload = ['message_id'=>$_message->id];
       return $response->withStatus(201)->withJson($payload);
   } else {
       return $response->withStatus(400); 
   } 

});

$app->delete('/messages/{message_id}', function($request, $response,$args){
  $_message = Message::find($args['message_id']);
  $_message->delete(); 
  if($_message->exists){
       return $response->withStatus(400);
   } else {
       return $response->withStatus(200);
   }

});

$app->put('/messages/{message_id}', function($request, $response,$args){
   $message = $request->getParsedBodyParam('message', '');
   $_message = Message::find($args['message_id']);
   //die("message id: ".$_message->id);
   $_message->body = $message;
   if($_message->save()){
       $payload = ['message_id'=>$_message->id, "result"=>"The message has been updated successfuly"];
       return $response->withStatus(200)->withJson($payload);
   } else{
       return $response->withStatus(400);  
   }

});


$app->post('/messages/bulk', function($request, $response,$args){
   $payload = $request->getParsedBody(); //convert the json to array
   Message::insert($payload); //incluse save
   return $response->withStatus(201)->withJson($payload);
     
});

$app->post('/auth', function($request, $response,$args){
   $user = $request->getParsedBodyParam('user', '');    
   $password = $request->getParsedBodyParam('password', '');  
   //we need to go to the db, but not now... 
   if($user == 'jack' && $password == '1234'){
       //we need to generate JWT but not now
       $payload = ['token' => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3IiwibmFtZSI6ImphY2siLCJhZG1pbiI6dHJ1ZX0.PETyPBssJTN1V8fSwPxkE0Hftg3hGYux4Rx1WcMC5NA'];
       return $response->withStatus(200)->withJson($payload);
   } else {
       $payload = ['token' => null];
       return $response->withStatus(403)->withJson($payload);
   } 
});

$app->add(new \Slim\Middleware\JwtAuthentication([
    "secret" => "supersecret",
    "path" => ['/messages'] //הראוטים שאנחנו רוצים לפקח עליהם
]));

//share data with other domains
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', /*'http://mysite'*/'*') // * is all the domains
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();